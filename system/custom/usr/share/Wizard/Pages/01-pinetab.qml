/*
 * Copyright (C) 2020 UBports Foundation
 *
 * Written by: Dalton Durst <dalton@ubports.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import AccountsService 0.1
import Wizard 0.1
import "file:///usr/share/unity8/Wizard" as LocalComponents

LocalComponents.Page {
    objectName: "pineTabWelcome"
    title: i18n.tr("Welcome to your PineTab Early Adopter Edition")
    id: pineTabWelcome

    skip: false

    // See skipTimer below for information about this hack
    property bool loading: false

    forwardButtonSourceComponent: forwardButton
    onlyOnUpdate: false

    ScrollView {
        id: scroll

        anchors {
            fill: content
            leftMargin: wideMode ? parent.leftMargin : staticMargin
            rightMargin: wideMode ? parent.rightMargin : staticMargin
        }

        Column {
            id: column

            width: scroll.width

            // Make it appear that the text is hiding behind the header
            Item {
                height: staticMargin
                width: units.gu(1)
            }

            Label {
                anchors {
                    // Keep the scroll bar from interfering with text
                    rightMargin: units.gu(1)
                }
                id: changelogText
                width: parent.width
                wrapMode: Text.WordWrap
                textSize: Label.Large
                text: "Congratulations on being one of the first people in the world to receive a PINE64 PineTab!

This version of Ubuntu Touch has several known issues that we were not able to handle before release. For that reason, it is a good idea to do the following now:

1. Remove the PineTab from the keyboard dock if it is attached
2. Connect to WiFi using the next pages of this Wizard
3. Head to Settings -> Updates and install all updates. You may need to repeat this step twice to receive an update to Ubuntu Touch.

This will ensure you have the latest and greatest software.

You may reconnect the keyboard dock after updates are complete.

You can learn more about this device by browsing to the following link after setup is complete:

https://wiki.pine64.org/PineTab/Early-Adopter
"
            }
        }
    }

    Component {
        id: forwardButton
        LocalComponents.StackButton {
            text: loading ? i18n.tr("Loading...") : i18n.tr("Next")
            onClicked: {
                pineTabWelcome.loading = true;
                skipTimer.restart();
            }
        }
    }

    // A horrible hack to make sure the UI refreshes before actually skipping
    // Without this, people press the Next button multiple times and skip
    // multiple pages at once.
    Timer {
        id: skipTimer
        interval: 100
        repeat: false
        running: false
        onTriggered: {
            pineTabWelcome.loading = false;
            pageStack.next();
        }
    }
}
