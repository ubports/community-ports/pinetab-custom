This repository builds the 'custom' tarball installed to the PINE64 PineTab's factory image.

This tarball provides the "Welcome to your PineTab" Wizard page on first boot.
